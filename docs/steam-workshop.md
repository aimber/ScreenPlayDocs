# 🎮 Steam Workshop Integration

## Overview

ScreenPlay integrates with Steam Workshop to allow easy sharing and discovery of wallpapers and widgets. This guide explains how to use the Workshop features effectively.

![Steam Workshop Interface](workshop.png)

## Publishing Content

### Prerequisites
* A Steam account
* ScreenPlay installed through Steam
* Content that meets our [project guidelines](projectfile.md)

### Steps to Publish
1. Create your wallpaper or widget
2. Test it locally to ensure everything works
3. Prepare your preview materials:
   * Main preview image (required)
   * Preview GIF (optional, max 4MB)
   * Preview video (optional, WebM format)
4. Open ScreenPlay's Workshop interface
5. Click "Upload"
6. Submit your content

### Subscribe to Content
1. Find content you like
2. Click "Subscribe"
3. Content will automatically download
4. Find downloaded items in your "Installed" tab

## Managing Workshop Content

### Current Limitations
* Content updates are not yet implemented
  * You cannot modify published content
  * To update content, you'll need to publish as new

### Best Practices
* Test thoroughly before publishing
* Provide clear descriptions
* Use appropriate tags
* Include setup instructions if needed
* Respond to comments and questions
* Join our [community forum](https://forum.screen-play.app/) for support

## Future Updates

We're actively working on improving Workshop integration. Planned features include:
* Content updating system
* Better content management tools
* Enhanced preview options
* Improved browsing and filtering
* Rating and review system

Stay tuned to our [announcements](https://forum.screen-play.app/) for updates!