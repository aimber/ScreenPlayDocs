# 🏗️ Architecture Overview
ScreenPlay uses a separate ScreenPlayWallpaper app to display the wallpaper. This has the advantage that if the wallpaper crashes, the main ScreenPlay app won't crash. ScreenPlay consists of 9 projects:

1. ScreenPlay (Executable)
    * The main ScreenPlay App UI with Create, Installed, Community and Settings.
1. ScreenPlayWallpaper (Executable)
    * The Wallpaper executable that is used for displaying a single wallpaper. This uses ScreenPlaySDK to talk via QLocalsockets with the main ScreenPlayApp.
1. ScreenPlayWidget (Executable)
    * The Widget executable that is used for displaying a single widget. This uses ScreenPlaySDK to talk via QLocalsockets with the main ScreenPlayApp.
1. ScreenPlayCore (Libray)
    * Core functioniallity, like project.json loading, that are needed in ScreenPlay and Workshop plugin.
1. ScreenPlaySDK (Libray)
    * A SDK used internally in the ScreenPlayWallpaper and ScreenPlayWidget to talk to the main ScreenPlay app via QLocalsockets (Windows Pipes and Unix sockets).
1. ScreenPlayShader (Libray)
    * A collection of reusable shader (work in progress)
1. ScreenPlaySysInfo (Libray)
    * A qml plugin to read CPU, GPU, Network and all sort of statisitcs.
1. ScreenPlayWeather (Libray)
    * A qml plugin to display weather infos.
2. ScreenPlayWorkshop (Libray)
    * The Steam workshop plugin that is loaded at runtime.

### ScreenPlay App Class
ScreenPlay main applications wrapps all classes into a App.h and App.cpp class. This is easier for unit test integration. The main app class is used for owning [all other classes via Q_PROPERTY](https://gitlab.com/kelteseth/ScreenPlay/-/blob/master/ScreenPlay/inc/public/ScreenPlay/app.h#L42) to be easily accessible in the GUI/QML. We use the [constructor](https://gitlab.com/kelteseth/ScreenPlay/-/blob/master/ScreenPlay/src/app.cpp#L66) of the App class to initialize all Qt register types, fonts and to check if another ScreenPlay instance is running. The [init()](https://gitlab.com/kelteseth/ScreenPlay/-/blob/master/ScreenPlay/src/app.cpp#L137) method is used to initialize  all other c++ classes in a certain order! It also sets up some events [signal/slots](https://doc.qt.io/qt-6/signalsandslots.html) like when the user changes the UI language.

For all other class documentation please visit [ScreenPlayDeveloperDocs](https://kelteseth.gitlab.io/ScreenPlayDeveloperDocs/)


