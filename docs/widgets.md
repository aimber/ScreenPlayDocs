# 🧩 Widgets
You can create Widgets via the build in wizard:


![RSS example](wizard.png "RSS  example")


Widgets are custom QML files which gets executed via the ScreenPlayWidget app. You can learn more about how to develop your widget in qml (even without prior rogramming skill!) via the fantastic qml book.

- [Getting started with qml](http://qmlbook.github.io/)


## Simple interactive button example

[Try this code in your browser!](https://tinyurl.com/y933aksb)

``` qml
import QtQuick
import QtQuick.Controls

Item {
    id:root
    anchors.fill:parent

    Rectangle {
        color: "#333333"
        width:300
        height:300
        opacity: 0.5
        anchors.centerIn: parent
        Button {
            id:txtHello
            text: qsTr("Hello")
            anchors.centerIn: parent
            onClicked:{
                txtHello.text = "Hello Clicked"
            }
        }
    }
}

```
