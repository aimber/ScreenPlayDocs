# 🤝 Contributing to ScreenPlay

## Getting Started

ScreenPlay follows the standard GitLab workflow: fork the repository, make your changes, and submit a merge request (similar to GitHub's pull requests).

## Development Workflow

### 1. Setup Your Environment
1. First, follow the setup instructions in our [⚙️ Building ScreenPlay](building-screenplay.md)

### 2. Choose What to Work On
You have several options:

* **Find an Existing Issue**: Browse our ["Accepting merge requests"](https://gitlab.com/kelteseth/ScreenPlay/issues?state=opened&label_name[]=Accepting+merge+requests&assignee_id=0&sort=weight) issues
  * Issues are weighted by difficulty - start with lower weights!
  * Check that no one else is already working on it
  * Verify the issue is still relevant
* **Your Own Ideas**: Have an improvement in mind? Create a new issue!

Need help? Don't hesitate to ask questions in the issue comments - we're here to help!

### 3. Development Process
1. Make your code changes
3. Open a merge request early
   * Mark as "Work in Progress" if not complete
   * Early feedback helps avoid wasted effort

### 4. Quality Checks
* Ensure CI builds pass
* Update translations for any new text
  * Use `qsTr("text")` macro
  * Full translation not required initially
  * Other contributors can help with translations
* Address reviewer feedback
  * Multiple review rounds may be needed
  * Be patient and responsive

## Coding Standards

### General Guidelines
1. Follow Qt conventions:
   * [C++ coding style](https://wiki.qt.io/Coding_Conventions)
   * [QML coding style](https://doc.qt.io/qt-6/qml-codingconventions.html)
2. Use Qt containers when available (QStrings, QVector, etc.)
3. Use C++23 smart pointers
4. Follow [standard git commit message format](https://gist.github.com/robertpainsi/b632364184e70900af4ab688decf6f53)

### Code Formatting
Use WebKit style formatting with QtCreator's Beautifier plugin:

1. Enable the plugin:
   * Help → About Plugins → Beautifier
2. Install [LLVM](http://releases.llvm.org/download.html)
3. Configure formatting:
   * Tools → Options → Beautifier → Clang format
   * Set predefined style to WebKit
4. Add keyboard shortcut:
   * Tools → Options → Environment → Keyboard
   * Search for "format"
   * Assign shortcut to ClangFormat, FormatFile

## Other Ways to Contribute

### Documentation
* Edit docs directly on GitLab (look for the pen icon)
* All diagrams should use [draw.io](http://draw.io/)
  * Free and open-source
  * Supports embedding source in PNGs

### Translations
We use Qt Linguist for translations:
* Find it in Qt SDK: `C:\Qt\6.XX.0\msvc2022_64\bin\linguist.exe`
* Edit `.ts` files for each language
* See our [detailed translation guide](https://forum.screen-play.app/topic/6/how-to-add-new-languages-to-screenplay-step-by-step-guide)

### Design
* We use [Affinity Designer](https://affinity.serif.com/en-gb/designer/)
* Submit design proposals:
  * Open issue with "Design" tag
  * Include mockups as PNG
  * Include source files (*.afdesign) if possible
* Other design tools are welcome

## Legal Information
ScreenPlay uses dual licensing to support Steam and third-party SDK integration:

* [AGPLv3 License](https://gitlab.com/kelteseth/ScreenPlay/-/tree/dev/Legal)
* [For regular contributors](https://gitlab.com/kelteseth/ScreenPlay/blob/dev/Legal/individual_contributor_license_agreement.md)
* [For members of a company](https://gitlab.com/kelteseth/ScreenPlay/blob/dev/Legal/corporate_contributor_license_agreement.md)