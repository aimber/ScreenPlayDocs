# 📁 Projectfile

Every installed entity has a project.json file, containing all necessaries information and settings. We use [JSON](https://learnxinyminutes.com/docs/json/) for our settings.

You can always manually create ScreenPlay content. ScreenPlay searches in every subfolder of you installed path for a project.json file. ScreenPlay should automatically detect changes in this path. If it's not working, you can always press F5 inside the Installed tab or restart ScreenPlay.

##### Example ScreenPlay Installed base path (672870 is the Steam App ID of ScreenPlay):

> C:\Program Files (x86)\Steam\steamapps\workshop\content\672870\

##### ScreenPlay Installed project inside the 2133699532 folder:

> C:\Program Files (x86)\Steam\steamapps\workshop\content\672870\2133699532\project.json

#### Typical content of a Wallpaper:

* project.json
* MyFancyWallpaper.webm
* audio.mp3
* preview.gif
* preview.jpg
* preview.webm
* previewThumbnail.jpg

A valid project contains a *project.json* inside a *subfolder* of your base path. The content of the project folder may vary, because Wallpaper and Widgets can have different types like videos, html website or QML 

An example  **project.json** file can look like this:
```javascript
{
    "description": "Quiet particles",
    "file": "main.qml",
    "title": "QML particles",
    "type": "qmlWallpaper",
    "visibility": "public",
    "workshopid": 12345678,
    "preview": "preview.png",
    "previewGIF": "preview.gif",
    "previewWEBM": "preview.webm",
    "tags": [
        "particles"
    ],
    "properties": {
        "Attractor": {
            "attStrength": {
                "from": 0,
                "stepSize": 100,
                "to": 100000,
                "type": "slider",
                "value": 8000000
            }
        },
        "Emitter": {
            "emitRate": {
                "from": 0,
                "stepSize": 1,
                "to": 2500,
                "type": "slider",
                "value": 25
            },
            "isEnabled": {
                "type": "bool",
                "value": true
            }
        },
        "ImageParticle": {
            "color": {
                "type": "color",
                "value": "#ffffff"
            },
            "imgOpacity": {
                "from": 0,
                "stepSize": 1,
                "to": 1,
                "type": "slider",
                "value": 1
            },
            "source": {
                "formats": "*.png,*.jpg",
                "type": "file",
                "value": "backgroundGlow.png"
            }
        }
    }
}
```
## Settings
#### Base settings
```javascript
    "description": "Quiet particles",
    "title": "QML particles",
    "youtube_previews": [
        "https://youtu.be/-Gqo1lD1skQ"
        "https://youtu.be/zlmqbNGW7JA"
    ]
```


#### Properties
Properties can have multiple types. For example it is useful to have a slider to control the volume and a file dialogue to set an image. Properties can have default values called **value**. Properties can be nested to be displaced clearer to the user. Nesting has no effect on the namepsace. This means **every property has to have an unique name!**
```javascript
// Uses a slider white an from, to and a stepSize value
"property_one": {
    "from": 0,
    "stepSize": 1,
    "to": 100,
    "type": "slider",
    "value": 50
},
// Uses a color picker
"property_two": {
    "type": "color",
    "value": "#ffffff"
},
// Uses a file open dialogue
"property_three": {
    "formats": "*.png,*.jpg",
    "type": "file",
    "value": "backgroundGlow.png"
},
// Uses a checkbox
"property_four": {
    "type": "bool",
    "value": true
}
```
##### Example
The name of the object then gets translated to the qml property. For example:
```javascript
    "attStrength": {
        "from": 0,
        "stepSize": 100,
        "to": 100000,
        "type": "slider",
        "value": 8000000
    }
```
```qml
// main.qml
property int attStrength: 8000000
```

#### Wallpaper File/Type options
Tell ScreenPlay what type of project we have:
```javascript
// A QML wallpaper:
"file": "main.qml",
"type": "qmlWallpaper"

// A HTML wallpaper:
"file": "index.html",
"type": "htmlWallpaper"

// A Video wallpaper:
"file": "video.webm",
"type": "videoWallpaper"

// A Gif wallpaper:
"file": "myGif.gif",
"type": "gifWallpaper"

// Displays a website as a wallpaper. Url _must_ begin with http:// or https:// !
"url": "https://screen-play.app/",
"type": "websiteWallpaper"

```

#### Widget File/Type options
Tell ScreenPlay what type of widget we have:
```javascript
// A QML widget:
"file": "main.qml",
"type": "qmlWidget"

// A HTML widget:
"file": "main.html",
"type": "htmlWidget"

```


#### Preview options 
```javascript
// Must be a jpg or png!
"preview": "preview.png",
// Must be a jpg or png. Used in ScreenPlay for displaying 320x121 preview.
// When not provided the pewview.png is used. Not recommneded, can slow down
// your installed list model!
"previewThumbnail": "previewThumbnail.png",
// Must be gif! (used in ScreenPlay hover preview)
// If you want to upload you content to Steam the max file size is 4mb!
"previewGIF": "preview.gif",
// Must be webm! (used in steam)
"previewWEBM": "preview.webm",
```
#### Steam workshop id
```javascript
// This gets added automatically when uploading your project to steam
"workshopid": 12345678,
```
