# 💻 System Informations API

## Sysinfo to display hardware stats
Just import the SysInfo project in the beginning of your QML file:

```qml
import ScreenPlaySysInfo
```
Example:
```qml
import QtQuick
import QtQuick.Controls
import ScreenPlaySysInfo

Item {
    id:root
    anchors.fill: parent

    Rectangle {
        color: "white"
        text: "My CPU Usage: " + SysInfo.cpu.usage 
        anchors.centerIn: parent
    }
}
```

```qml
 // CPU
 - SysInfo.cpu.usage 
 - SysInfo.cpu.tickRate //Update every second

 // RAM
 - SysInfo.ram.usage

 - SysInfo.ram.usedPhysicalMemory
 - SysInfo.ram.totalPhysicalMemory

 - SysInfo.ram.usedVirtualMemory
 - SysInfo.ram.totalVirtualMemory

 - SysInfo.ram.usedPagingMemory
 - SysInfo.ram.totalPagingMemory

  // Storage (list model)
  // These variables are available for every listed storage device
  - name
  - displayName
  - isReadOnly
  - isReady
  - isRoot
  - isValid
  - bytesAvailable
  - bytesTotal
  - bytesFree
  - fileSystemType
```


### Sysinfo platform support

| Feature                	| Windows 	| Linux 	| MacOS 	|
|------------------------	|---------	|-------	|-------	|
| __CPU__              	    | ✔       	| ❌       | ❌     	|
| __RAM__                   | ✔       	| ❌       | ❌     	|
| __Storage__               | ✔        	|  ✔      |   ✔   	|
| __Network__               | ✔        	|  ✔     |  ✔     	|
| __GPU__       	        | ✔        	| ❌    	 |     ❌  	|

