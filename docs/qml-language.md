# 📝 Qml Language


## Introduction
QML (Qt Modeling Language) is a modern UI design language that will feel familiar if you know HTML, CSS, and JavaScript. It combines the best aspects of web technologies into one elegant framework, making it perfect for creating ScreenPlay widgets and wallpapers.

## Comparing with Web Technologies

### QML vs Traditional Web Development

| Web Technology | QML Equivalent | Advantage |
|---------------|----------------|-----------|
| HTML Elements | QML Items | More powerful built-in components |
| CSS | Property assignments | Styling and layout in one place |
| JavaScript | JavaScript/QML logic | Tighter integration with UI |

### Basic Structure
Here's a simple QML file:

```qml
import QtQuick

// QML requires exactly one top-level element
Item {  
    // ScreenPlay handles the root element sizing
    anchors.fill: parent
    
    Rectangle {  // Like a <div>
        color: "steelblue"
        anchors.centerIn: parent
        width: 200
        height: 100
        
        Text {  // Like a <p> or <span>
            text: "Hello World!"
            color: "white"
            anchors.centerIn: parent
        }
    }
}
```

Note: Every QML file must have exactly one root element. In ScreenPlay widgets and wallpapers, this root element should use `anchors.fill: parent` to properly fill the available space.

## Key Features

### 1. Declarative and Clean
Just describe what you want, not how to do it:

```qml
Rectangle {
    // Properties automatically update when values change
    width: parent.width * 0.5
    color: mouseArea.pressed ? "red" : "blue"
}
```

### 2. Native Performance
Unlike web apps, QML compiles to native code for:
- Smooth animations
- Efficient rendering
- Better system resource usage

### 3. Built-in Animation System
```qml
Rectangle {
    // Smooth animation with minimal code
    Behavior on width {
        NumberAnimation { duration: 300 }
    }
}
```

### 4. Easy System Integration
ScreenPlay provides APIs for system monitoring:

```qml
import QtQuick
import ScreenPlaySysInfo

Text {
    text: "CPU: " + SysInfo.cpu.usage + "%"
    // Updates automatically!
}
```

## Layout System

### Centering Elements
No more CSS flexbox or grid struggles! In QML, centering is straightforward:

```qml
// Center in parent
Rectangle {
    anchors.centerIn: parent  // That's it! Both horizontal and vertical
    width: 100
    height: 100
}

// Center horizontally only
Rectangle {
    anchors.horizontalCenter: parent.horizontalCenter
    width: 100
    height: 100
}

// Center vertically only
Rectangle {
    anchors.verticalCenter: parent.verticalCenter
    width: 100
    height: 100
}
```

### Built-in Layout Types
QML provides powerful layout elements:

```qml
// Row: Horizontal layout (like CSS flexbox row)
Row {
    spacing: 10  // Gap between items
    
    Rectangle { width: 50; height: 50; color: "red" }
    Rectangle { width: 50; height: 50; color: "blue" }
    Rectangle { width: 50; height: 50; color: "green" }
}

// Column: Vertical layout (like CSS flexbox column)
Column {
    spacing: 10
    
    Text { text: "First" }
    Text { text: "Second" }
    Text { text: "Third" }
}

// Grid: 2D layout (simpler than CSS grid!)
Grid {
    columns: 2
    spacing: 10
    
    Rectangle { width: 50; height: 50; color: "red" }
    Rectangle { width: 50; height: 50; color: "blue" }
    Rectangle { width: 50; height: 50; color: "green" }
    Rectangle { width: 50; height: 50; color: "yellow" }
}

// Flow: Responsive layout that wraps items
Flow {
    width: parent.width
    spacing: 10
    
    Repeater {
        model: 5
        Rectangle {
            width: 100
            height: 100
            color: Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
        }
    }
}
```

### Anchors System
QML's anchor system is more intuitive than CSS positioning:

```qml
Rectangle {
    id: blueRect
    color: "blue"
    width: 100
    height: 100
    anchors {
        right: parent.right    // Align to parent's right
        bottom: parent.bottom  // Align to parent's bottom
        margins: 10           // Margin on all sides
    }
}

Rectangle {
    color: "red"
    width: 100
    height: 100
    anchors {
        left: blueRect.right   // Position relative to blue rectangle
        leftMargin: 10         // Specific margin for one side
        verticalCenter: blueRect.verticalCenter  // Align centers vertically
    }
}
```

### Responsive Layouts
Making elements respond to their container is simple:

```qml
Rectangle {
    // Take up 80% of parent width
    width: parent.width * 0.8
    // But never get bigger than 500 pixels
    width: Math.min(parent.width * 0.8, 500)
    // Maintain aspect ratio
    height: width * 0.75
    
    anchors.centerIn: parent
}
```

## Creating Widgets

### Simple Widget Example
Here's a complete widget that shows system information:

```qml
import QtQuick
import QtQuick.Controls
import ScreenPlaySysInfo

Item {
    anchors.fill: parent
    
    Rectangle {
        color: "#333333"
        opacity: 0.8
        anchors.centerIn: parent
        width: 200
        height: 100
        
        Column {
            anchors.centerIn: parent
            spacing: 10
            
            Text {
                color: "white"
                text: "CPU: " + SysInfo.cpu.usage + "%"
            }
            
            Text {
                color: "white"
                text: "RAM: " + SysInfo.ram.usage + "%"
            }
        }
    }
}
```

## Advanced Features

### Visual Effects
QML supports advanced features for creating rich wallpapers:

- Particle systems for effects
- Shader effects for custom visuals
- 3D rendering capabilities
- Touch and gesture handling
- Hardware acceleration

## Best Practices

### Layout Guidelines
1. **Use Anchors for Layout**
   - More flexible than fixed positioning
   - Automatically adjusts to different screen sizes

2. **Property Bindings**
   - Use them for dynamic values
   - Helps create reactive UIs

3. **Performance**
   - Use Images instead of gradients for complex backgrounds
   - Limit the number of animated items
   - Use visible: false instead of opacity: 0 when hiding items

## Getting Started

### Learning Resources
1. Start with simple widgets to learn the basics
2. Experiment with the ScreenPlay API
3. Look at example widgets in the ScreenPlay documentation
4. Try the [QML Book](https://www.qt.io/product/qt6/qml-book) for in-depth learning

Remember: QML makes it easy to start simple and gradually add complexity as needed. You can create basic widgets with just a few lines of code, then expand them with animations and interactivity as you learn more